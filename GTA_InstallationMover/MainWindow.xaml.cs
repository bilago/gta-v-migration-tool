﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Linq;

namespace GTA_InstallationMover
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private readonly BackgroundWorker _worker = new BackgroundWorker();
        private readonly BackgroundWorker _delworker = new BackgroundWorker();

        private void delworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetStatus("Migration is complete!");
            if (ForceVerify.IsChecked ?? false)
                VerifyLauncher();
            SetStart(true);
        }

        private void delworker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!_noErrors)           
                MessageBox.Show("There were errors during transfer, skipping file deletion. Please verify the game is working from the new directory and delete the files manually.");
            else
            {
                if (MessageBox.Show(string.Format("You selected to delete all files in {0}. Are you sure?", CurrentInstallationPath), "Preparing to delete files...", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    SetStatus("Operation was aborted!");
                    SetStart(true);
                    return;
                }
                foreach (var newPath in Directory.GetFiles(CurrentInstallationPath, "*.*",
                    SearchOption.AllDirectories))
                {
                    //File.Copy(newPath, newPath.Replace(CurrentInstallationPath, NewInstallationPath), true);
                    try
                    {
                        SetStatus(string.Format("Deleting file {0}", newPath));
                        File.Delete(newPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        _noErrors = false;
                    }
                }

                SetStatus(string.Format("Deleting entire Directory and subdirectories in {0}", CurrentInstallationPath));
                Directory.Delete(CurrentInstallationPath, true);
            }
            SetStatus("Migration is complete!");            
            SetStart(true);
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = Directory.GetFiles(CurrentInstallationPath, "*.*",
                SearchOption.AllDirectories);
            //Copy all the files & Replaces any files with the same name
            foreach (var newPath in files)
            {
                try
                {
                    //File.Copy(newPath, newPath.Replace(CurrentInstallationPath, NewInstallationPath), true);
                    SetStatus(string.Format("Copying {0} to {1}", newPath, _newInstallationPath));
                    
                    File.Copy(newPath, newPath.Replace(CurrentInstallationPath, NewInstallationPath), true);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    _noErrors = false;
                }
            }

        }

        private void SetStatus(string text)
        {
            Status.Dispatcher.Invoke((Action)(() => Status.Text = text));
        }

        private void SetStart(bool enabled)
        {
            Start.Dispatcher.Invoke((Action)(()=> Start.IsEnabled=enabled));
        }

        private static void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
         
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            Status.Text = "Copying is complete, changing Registry key.";
            InstallPath(true);
            if (DeleteOriginal.IsChecked ?? false)
            {
                _delworker.RunWorkerAsync();
                return;
            }
            else
            {
                Status.Text = "Migration is complete!";
                if (ForceVerify.IsChecked ?? false)
                    VerifyLauncher();
                    
                SetStart(true);
            }
        }

        private void VerifyLauncher()
        {
            try
            {
                System.Diagnostics.ProcessStartInfo pinfo = new System.Diagnostics.ProcessStartInfo();
                pinfo.Arguments = "-verify";
                pinfo.WorkingDirectory = NewInstallationPath;
                pinfo.FileName = System.IO.Path.Combine(NewInstallationPath, "GTAVLauncher.exe");
                System.Diagnostics.Process.Start(pinfo);
            }
            catch
            {
                MessageBox.Show("There was an error starting the launcher. Please verify manually.");
            }
        }

        private readonly System.Windows.Forms.FolderBrowserDialog _dialog = new System.Windows.Forms.FolderBrowserDialog();

        private string _installationPath;

        private string CurrentInstallationPath
        {
            get { return _installationPath; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    _installationPath = value;
                    CurrentPath.Text = value;
                    Start.IsEnabled = true;
                }
                else
                    Start.IsEnabled = false;
            }
        }

        private string _newInstallationPath;

        private string NewInstallationPath
        {
            get { return _newInstallationPath; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    _newInstallationPath = value;
                    NewLocation.Text = value;
                }
                else
                    NewLocation.Text = "Click here to set a new location";
            }
        }

        public MainWindow()
        {

            InitializeComponent();
            CurrentInstallationPath = InstallPath(false);
            if (!Directory.Exists(CurrentInstallationPath))
            {
                Start.IsEnabled = false;
                Status.Text = "Installation is not detected on this machine.";
            }
            _worker.DoWork += worker_DoWork;
            _worker.ProgressChanged += worker_ProgressChanged;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _delworker.DoWork += delworker_DoWork;
            _delworker.RunWorkerCompleted += delworker_RunWorkerCompleted;
            _delworker.ProgressChanged += delworker_ProgressChanged;
        }

        static void delworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ChangeInstallPath()
        {
            try
            {
                if (!Directory.Exists(_newInstallationPath))
                {
                    MessageBox.Show("New Installation location does not exist! Aborting.");
                    return;
                }

                InstallPath(true);
                Status.Text = "Registry change is complete";

                if (DeleteOriginal.IsChecked ?? false)
                    Status.Text = "Deleting original installation...";
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fatal Error: {0}\nReport this message to the developer for help.", ex.Message));
                this.Close();
            }
        }

        private string InstallPath(bool edit)
        {
            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rockstar games\Grand Theft Auto V", edit))
                {

                    var path = string.Empty;
                    if (key != null)
                    {
                        if (edit)
                            key.SetValue("InstallFolder", _newInstallationPath);

                        path = key.GetValue("InstallFolder", "").ToString();
                    }
                    else
                        using (var newKey = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\", true))
                        {
                            if (newKey != null)
                            {
                                newKey.CreateSubKey(@"Rockstar Games\Grand Theft Auto V");
                                newKey.OpenSubKey(@"Rockstar Games\Grand Theft Auto V", true)
                                    .SetValue("InstallFolder", NewInstallationPath);
                                path =
                                    newKey.OpenSubKey(@"Rockstar Games\Grand Theft Auto V")
                                        .GetValue("InstallFolder", "")
                                        .ToString();
                            }
                        }

                    return string.IsNullOrWhiteSpace(path) ? "GTA not Detected As Installed! Click here to create the registry entry" : path;
                }
            }
            catch
            {
                MessageBox.Show("There was an error reading/writing to the registry. You must run this from an account with administrative privileges!");
                return string.Empty;
            }
        }        

        private void NewLocation_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.DialogResult result = _dialog.ShowDialog();
            if (result != System.Windows.Forms.DialogResult.Cancel)            
                NewInstallationPath = _dialog.SelectedPath;            
            else
                return;

            Start.IsEnabled = true;
            if (!Directory.Exists(NewInstallationPath))
            {
                PromptToCreate(NewInstallationPath);
            }        
        }
        private bool _noErrors = true;
        private static bool PromptToCreate(string path)
        {
            try
            {
                if (MessageBox.Show(string.Format("Location {0} does not exist, would you like to create it?", path), "Directory not found", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Directory.CreateDirectory(path);
                    return true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error creating directory");
                
            }
            return false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Directory.Exists(CurrentInstallationPath))
                {
                    MessageBox.Show("Installation path does not exist! Is GTAV Installed?");
                    return;
                }

                if (string.IsNullOrEmpty(_newInstallationPath))
                {
                    MessageBox.Show("Please set a new install directory to continue", "No new install path detected");
                    return;
                }
                if (!Directory.Exists(NewInstallationPath))
                    if (!PromptToCreate(NewInstallationPath))
                    {
                        MessageBox.Show("Installation migration aborted, destination does not exist!");
                        return;
                    }

                StringBuilder build = new StringBuilder();
                if (MoveFiles.IsChecked ?? false)
                    build.Append("-Move files to new directory\n");

                if (DeleteOriginal.IsChecked ?? false)
                    build.Append("-Delete Original Files after move\n");

                if (ForceVerify.IsChecked ?? false)
                    build.Append("-Force Verification of game files after move\n");

                if (MessageBox.Show("You are about the start the following tasks:\n\n-Change the Registry Installation Folder\n" + build + "\nContinue?\nBy Pressing YES you agree that you understand how this software works.", "Starting Migration...", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    SetStatus("User Aborted Migration!");
                    return;
                }
                Start.IsEnabled = false;


                if (!MoveFiles.IsChecked ?? false)
                {
                    Status.Text = "Changing Installation Path in registry...";
                    Status.Text = string.IsNullOrWhiteSpace(InstallPath(true)) ? "Operation failed!" : "Operation is complete!";

                    Start.IsEnabled = true;
                    return;
                }
                else
                    moveFiles();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fatal Error: {0}\nReport this message to the developer for help.", ex.Message));
                this.Close();
            }
           
        }

        private void moveFiles()
        {
            try
            {
                var del = DeleteOriginal.IsChecked ?? false;
                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(CurrentInstallationPath, "*",
                    SearchOption.AllDirectories).Where(dirPath => dirPath != CurrentInstallationPath))
                {
                    Directory.CreateDirectory(dirPath.Replace(CurrentInstallationPath, NewInstallationPath));
                }

                _worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fatal Error: {0}\nReport this message to the developer for help.", ex.Message));
                this.Close();
            }
        }

        private void MoveFiles_Checked(object sender, RoutedEventArgs e)
        {
            if (MoveFiles.IsChecked ?? false)            
                DeleteOriginal.IsEnabled = true;            
            else
            {
                DeleteOriginal.IsChecked = false;
                DeleteOriginal.IsEnabled = false;
            }
        }

        private void CurrentPath_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!CurrentPath.Text.Contains("Click here"))
                return;

            if (MessageBox.Show("You do not have the registry key for this game on your system. Do you want to create this key? After pressing Yes, choose your GTA installation folder.", "Key missing from registry", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.No)
                return;

            if (_dialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            CurrentInstallationPath = _dialog.SelectedPath;
            NewInstallationPath = _dialog.SelectedPath;
            InstallPath(true);
            MessageBox.Show("Please restart this program to continue.");
            this.Close();            
        }

    }
}
